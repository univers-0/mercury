provider "aws" {
    profile = "default"
    region = "us-east-1"
    # It can pick the default region set in the default profile unless stated here explicitly
    
}

# Creating variables to be referenced by the resources below
variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}
# we use this var to predefine the subnet region at vars file.
# vpc is built on the default region set on the AWS configure file
# We can also set the region at the providers tab
variable my_ip {}

# Creating a VPC
resource "aws_vpc" "testapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = "${var.env_prefix}-vpc"
        # We glue a variable to a string using the ${var.variable-name} format
    }
}

# Creating a Subnet within the VPC we create
resource "aws_subnet" "testapp-vpc-subnet" {
vpc_id = aws_vpc.testapp-vpc.id
   cidr_block = var.subnet_cidr_block
   availability_zone = var.avail_zone
   tags = {
        Name = "${var.env_prefix}-subnet-1"
    } 
}

# Creating a Internet Gateway for our VPC
resource "aws_internet_gateway" "testapp-igw" {
    vpc_id = aws_vpc.testapp-vpc.id
    tags = {
        Name = "${var.env_prefix}-igw"
    }
}

# Setting default route table with internet gateway and a name
resource "aws_default_route_table" "testapp-rtdef" {
    default_route_table_id = aws_vpc.testapp-vpc.default_route_table_id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.testapp-igw.id
    }
    tags = {
        Name = "${var.env_prefix}-rt"
    }
} 

/*
# creating a security group with SSH ingress (inbound) and egress (outbound) rule
resource "aws_security_group" "testapp-sg" {
    vpc_id = aws_vpc.testapp-vpc.id

# This rule allows SSH from specific IP to the VPC-server
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }

# This rule allows access to VPC-server via browser
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

# This rule allows outgoing access from VPC-server. If we install anything on the server,
# it might look for extra files via different ports. So we want to set it open.
# This is done by setting ports to 0 for all ports, and -1 for all protocols
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
        # this one allows us to link to any VPC endpoint
    }

    tags = {
        Name = "${var.env_prefix}-sg"
    }

}
*/

# In order to use the default security group (always create new resoruces)
resource "aws_security_group" "testapp--def-sg" {
# name = "testapp-sg"
    vpc_id = aws_vpc.testapp-vpc.id

   ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
        # this one allows us to link to any VPC endpoint
    }

    tags = {
        Name = "${var.env_prefix}-def-sg"
    }

}